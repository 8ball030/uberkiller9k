# README #

Ride sharing agents for fetch ai 0x03

### What is this repository for? ###
Concept Of Process

![Alt text](docs/images/processFlow.png?raw=true "Title")

Architecture

![Alt text](docs/images/architecture.png?raw=true "Title")


### How do I get set up? ###

* Summary of set up

clone respository

```
git clone https://8ball030@bitbucket.org/8ball030/uberkiller9k.git
# change to diectory
cd uberkiller9k
# build the containers and start the project
docker-compose up
```

* Deployment instructions

### navigate to http://localhost:3001 ###

This will open the main simulation screen. 

Buttons to spawn drivers and passengers are included

* Create Drivers before Passengers

* NOTE it can take a couple of seconds for the agents to recieve its ID from the OEF.

* The apis to create agents can be directly accesed from the swagger pages available at;

http://localhost:5001 - Passengers
http://localhost:5002 - Drivers
http://localhost:5003 - Contracts

* this allows the user to directly modify the attributes and preferences of the agents

* NOTE - we connect our agents to the OEF so if there are multiple instances of the project open, the controller will not be able to update remote agents location.


