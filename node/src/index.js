import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
var lat = 52.051532,
    long = 1.154489,
    radius = 0.00001;

class PassengerPreference extends React.Component {
  constructor(props) {
    super(props);
    this.state = {startLong: null, startLat: null, endLong: null, endLat: null, preferences: {carStyle: null, driverScore: null, cost: null, wait: null}};
  }

  render() {
    return (
      <div className="passengerPreferences">
        <Map />
        <div className="preferences">
          {Object.keys(this.state.preferences).map(function(key) {
            return <SliderPreference name={key} />;
          })}
        </div>
      </div>
    );
  }
}

class Map extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.ctx = this.canvas.getContext('2d');
    this.image.addEventListener('load', function () {this.ctx.drawImage(this.image, 0, 0)}.bind(this));
  }

  componentDidUpdate() {
    this.ctx.drawImage(this.image, 0, 0);
    this.props.entities.drivers.forEach(this.drawEntity.bind(this, '36, 48, 214'));
    this.props.entities.passengers.forEach(this.drawEntity.bind(this, '217, 9, 78'));
  }

  drawEntity(color, entity) {
    let x = ((entity.currentLocationLat - (lat - radius)) / (radius * 2)) * 512;
    let y = ((entity.currentLocationLong - (long - radius)) / (radius * 2)) * 512;
    this.ctx.beginPath();
    this.ctx.arc(Math.floor(x), Math.floor(y), 10, 0, 2 * Math.PI);
    this.ctx.fillStyle = 'rgba(' + color + ', 0.5)'
    this.ctx.strokeStyle = 'rgba(' + color + ', 1)'
    this.ctx.fill();
    this.ctx.stroke();
  }

  render() {
    return (
      <div className="map">
        <h3>Map</h3>
        <canvas id="mapCanvas" width="512" height="512" ref={c => this.canvas = c}></canvas>
        <div style={{display:'none'}}>
          <img id="mapSource" src="image/bigmap.png" ref={c => this.image = c}></img>
        </div>
      </div>
    );
  }
}

class SliderPreference extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: null};
  }

  render() {
    return (
      <div className="slider">
        <input type="range" id={this.props.name} name={this.props.name} onChange={(e) => {this.setState({value: e.target.value})}} min="0" max="10"></input>
        <label for={this.props.name}>{this.props.name}</label>
      </div>
    );
  }
}

class EntitiesTable extends React.Component {
  constructor(props) {
    super(props);
    setInterval(this.props.update, 50)  }

  postEntity() {
    let nodes = [[0,62], [235, 107], [312, 65], [420, 0], [0, 280], [172, 225], [355, 275], [427, 235], [512, 190], [0, 355], [193, 352], [242, 430], [457, 315], [512, 290], [512, 340], [512, 385], [240, 330], [280, 502]];
    let node = nodes[Math.floor(Math.random() * nodes.length)];
    let names = ['Arlena', 'Martha', 'Mallie', 'Cletus', 'Alissa', 'Lindsay', 'Terrence', 'Lulu', 'Hobert', 'Gladis', 'Sabra', 'Britteny', 'Margret', 'Marcelina', 'Sirena', 'Letisha', 'Rowena', 'Siu', 'Piper', 'Joaquina', 'Sammie', 'Cheryle', 'Maryanne', 'Kathleen', 'Tyson', 'Matilde', 'Shea', 'Anastacia', 'Babette', 'Loreen', 'Annemarie', 'Randolph', 'Gaylene', 'Ben', 'Florine', 'Heidi', 'Devon', 'Nanette', 'Janita', 'Debi', 'Earlean', 'Lexie', 'Leona', 'Earline', 'Whitney', 'Graig', 'Walter', 'Gary', 'Edyth', 'Liz'];
    fetch(this.props.url, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: this.props.create()
    })
    .then(this.props.update);
  }

  render() {
    let button = '';
    if (!this.props.nocreate) {
      button=<button onClick={this.postEntity.bind(this)}>Create</button>;
    }
    return (
      <div className="entityTable" name={this.props.name}>
        <h3>{this.props.label}</h3>
        {button}
        <table>
          <thead><tr>{this.props.fields.map(f => <th>{f}</th>)}</tr></thead>
          <tbody>{this.props.entities.map((e) => <tr>{this.props.fields.map(f => <td>{e[f]}</td>)}</tr>)}</tbody>
        </table>
      </div>
    );
  }
}

class SimulationPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      drivers: [],
      passengers: [],
      contracts: []
    };
    this.fetchDrivers = this.fetchEntities.bind(this, 'drivers');
    this.fetchPassengers = this.fetchEntities.bind(this, 'passengers');
    this.createDrivers = this.createEntity.bind(this, 'drivers');
    this.createPassengers = this.createEntity.bind(this, 'passengers');
    this.fetchContracts = this.fetchEntities.bind(this, 'contracts');
  }

  componentDidMount() {
    this.fetchEntities();
  }

  createEntity(entity) {
    let nodes = [[0,62], [235, 107], [312, 65], [420, 0], [0, 280], [172, 225], [355, 275], [427, 235], [512, 190], [0, 355], [193, 352], [242, 430], [457, 315], [512, 290], [512, 340], [512, 385], [240, 330], [280, 502]];
    let node = nodes[Math.floor(Math.random() * nodes.length)];
    let names = ['Arlena', 'Martha', 'Mallie', 'Cletus', 'Alissa', 'Lindsay', 'Terrence', 'Lulu', 'Hobert', 'Gladis', 'Sabra', 'Britteny', 'Margret', 'Marcelina', 'Sirena', 'Letisha', 'Rowena', 'Siu', 'Piper', 'Joaquina', 'Sammie', 'Cheryle', 'Maryanne', 'Kathleen', 'Tyson', 'Matilde', 'Shea', 'Anastacia', 'Babette', 'Loreen', 'Annemarie', 'Randolph', 'Gaylene', 'Ben', 'Florine', 'Heidi', 'Devon', 'Nanette', 'Janita', 'Debi', 'Earlean', 'Lexie', 'Leona', 'Earline', 'Whitney', 'Graig', 'Walter', 'Gary', 'Edyth', 'Liz'];
    let body = {
        id: 0,
        name: names[Math.floor(Math.random() * names.length)],
        currentLocationLat: ((node[0] + (-5 + (Math.random() * 10))) / 512) * (radius * 2) + (lat -radius),
        currentLocationLong: ((node[1] + (-5 + (Math.random() * 10))) / 512) * (radius * 2) + (long -radius),
        carStylePreference: "" + Math.floor(Math.random() * 10),
        driverCommunityScorePreference: "" + Math.floor(Math.random() * 10),
        costPreference: "" + Math.floor(Math.random() * 10),
        status: 'available'
    };
    if (entity == 'drivers') {
        body.homeLocationLat = Math.random() * (radius * 2) + (lat - radius);
        body.homeLocationLong = Math.random() * (radius * 2) + (long - radius);
        body.finalDestinationFromHomePreference = "" + Math.floor(Math.random() * 10);
    } else if (entity == 'passengers') {
        let end = nodes[Math.floor(Math.random() * nodes.length)];
        body.endLocationLat = ((end[0] + (-5 + (Math.random() * 10))) / 512) * (radius * 2) + (lat -radius);
        body.endLocationLong = ((end[1] + (-5 + (Math.random() * 10))) / 512) * (radius * 2) + (long -radius);
        body.waitTimePreference = "" + Math.floor(Math.random() * 10);
    }
    return JSON.stringify(body);
  }

  fetchEntities(entity) {
    entity = entity || ['drivers', 'passengers', 'contracts'];
    if (!Array.isArray(entity)) {
      entity = [entity];
    }
    entity.forEach(e => {
      fetch(this.props[e])
      .then(results => results.json())
      .then(data => {
        let state = this.state;
        state[e] = data;
        this.setState(state);
      })
    });
  }

  render() {
    return (
      <div className="simulation">
        <Map entities={this.state}/>
        <EntitiesTable label="Drivers" name="drivers" fields={['id', 'name', 'status']} entities={this.state.drivers} url={this.props.drivers} update={this.fetchDrivers} create={this.createDrivers}/>
        <EntitiesTable label="Passengers" name="passengers" fields={['id', 'name']} entities={this.state.passengers} url={this.props.passengers} update={this.fetchPassengers} create={this.createPassengers}/>
        <EntitiesTable label="Contracts" name="contracts" fields={['id', 'driverId', 'passengerId', 'status', 'cost']} entities={this.state.contracts} url={this.props.contracts} update={this.fetchContracts} nocreate={true}/>
      </div>
    );
  }
}


// ========================================
var driverUrl = "http://" + window.location.hostname + ":5002/Drivers/";
var passengerUrl = "http://" + window.location.hostname + ":5001/Passengers/";
var contractUrl = "http://" + window.location.hostname + ":5003/Contracts/";

ReactDOM.render(
  <SimulationPage drivers={driverUrl} passengers={passengerUrl} contracts={contractUrl} />,
  document.getElementById('root')
);

