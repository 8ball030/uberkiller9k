from flask import Flask
from flask_restplus import Api, Resource, fields
from werkzeug.contrib.fixers import ProxyFix
from threading import Thread
# https://flask-restplus.readthedocs.io/en/stable/example.html
app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='PassengerMVC API',
    description='A simple PassengerMVC API',
)
from .fetch.client_agent import makeAgent
ns = api.namespace('Passengers', description='Passenger operations')



from flask_cors import CORS
CORS(app)


Passenger = api.model('Passenger', {
    'id': fields.String(readOnly=True,
                         description='The Passenger unique identifier'),
    'name': fields.String(required=True,
                          description='The Passengers Name'),
    'currentLocationLat': fields.Float(required=True,
                          description='The Passengers current Latitude'),

    'currentLocationLong': fields.Float(required=True,
                          description='The Passenger current Longitude'),

    'endLocationLat': fields.Float(required=True,
                          description='The Passengers current Latitude'),

    'endLocationLong': fields.Float(required=True,
                          description='The Passenger current Longitude'),

    'carStylePreference': fields.Float(required=True,
                         description='The Passenger unique identifier'),

    'driverCommunityScorePreference': fields.Float(required=True,
                          description='The Passengers Name'),

    'costPreference': fields.Float(required=True,
                          description='The Passengers Name'),

    'waitTimePreference': fields.Float(required=True,
                          description='The Passengers Name'),

})



class PassengerDAO(object):
    def __init__(self):
        self.counter = 0
        self.Passengers = []
        self.OEF_AGENTS = {}

    def get(self, id):
        for Passenger in self.Passengers:
            if Passenger['id'] == id:
                return Passenger
        api.abort(404, "Passenger {} doesn't exist".format(id))

    def create(self, data):
        Passenger = data
        print(data)
        Passenger['id'] = self.counter = self.counter + 1
        
        # here we create the agent for the passenger
        agent, idd = makeAgent(data)
        Passenger['id'] = idd
        Thread(target=agent.run).start()
        
        self.OEF_AGENTS[idd] = agent
        self.Passengers.append(Passenger)
        
        return Passenger

    def update(self, id, data):
        Passenger = self.get(id)
        Passenger.update(data)
        self.OEF_AGENTS[id].preferences.update(data)
        return Passenger

    def delete(self, id):
        Passenger = self.get(id)
        self.Passengers.remove(Passenger)



DAO = PassengerDAO()

@ns.route('/')
class PassengerList(Resource):
    '''Shows a list of all Passengers, and lets you POST to add new Passengers'''
    @ns.doc('list_Passengers')
    @ns.marshal_list_with(Passenger)
    def get(self):
        '''List all Passengers'''
        return DAO.Passengers

    @ns.doc('create_Passenger')
    @ns.expect(Passenger)
    @ns.marshal_with(Passenger, code=201)
    def post(self):
        '''Create a new Passenger'''
        return DAO.create(api.payload), 201


@ns.route('/<string:id>')
@ns.response(404, 'Passenger not found')
@ns.param('id', 'The Passenger identifier')
class Passenger(Resource):
    '''Show a single Passenger item and lets you delete them'''
    @ns.doc('get_Passenger')
    @ns.marshal_with(Passenger)
    def get(self, id):
        '''Fetch a given resource'''
        return DAO.get(id)

    @ns.doc('delete_Passenger')
    @ns.response(204, 'Passenger deleted')
    def delete(self, id):
        '''Delete a Passenger given its identifier'''
        DAO.delete(id)
        return '', 204

    @ns.expect(Passenger)
    @ns.marshal_with(Passenger)
    def put(self, id):
        '''Update a Passenger given its identifier'''
        return DAO.update(id, api.payload)


if __name__ == '__main__':
    app.run(debug=True)
