#!/usr/bin/env python
# coding: utf-8

# In[1]:


import string
import networkx
import random

# In[120]:


mapp = networkx.DiGraph()




def genRanEdges(idx, noOCons=5):
    return [(idx, random.choice(list(mapp.nodes))) for x in range(noOCons)]
        

for x in string.ascii_uppercase[:20]:
    mapp.add_node(x)

for x in string.ascii_uppercase[:20]:
    mapp.add_edges_from(genRanEdges(noOCons=random.randint(1,2), idx=x))


# In[108]:


import random
random.choices


# In[109]:


genRanEdges("a")


# In[110]:


random.choice(list(mapp.nodes))


# In[111]:



# In[112]:


x = list(mapp.edges)

x


# In[123]:


import matplotlib.pyplot as plt
G = mapp
nx = networkx
pos = networkx.spring_layout(G)
nx.draw_networkx_nodes(G, pos, cmap=plt.get_cmap('jet'), 
                        node_size = 10)
nx.draw_networkx_labels(G, pos)
nx.draw_networkx_edges(G, pos, edgelist=mapp.edges, edge_color='r', arrows=False)
plt.show()


# In[ ]:




